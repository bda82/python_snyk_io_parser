from snyk_controller import SNYKController

sc = SNYKController()
sc.update_all()
v = sc.snyk_database

print("\n\n")
print(f"SNYK Parsing complete. Append {len(v)} vulnerabilities")
print("\n\n")