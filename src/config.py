

class Configuration:

    min_sleep_time = 2
    max_sleep_time = 7

    huge_sleep_time = 60 * 15
    retry_count = 5

    content_stopwords = frozenset(["Overview", "Remediation", "References"])

    xpath_package_name = "/html/body/div[1]/main/div[3]/div[1]/nav/ol/li[3]"
    xpath_versions = "/html/body/div[1]/main/div[3]/div[2]/div[1]/p/strong[2]"
    xpath_cvss_crore = "/html/body/div[1]/main/div[4]/div[2]/div/div[1]/div/div/header/div[1]/div"
    xpath_severity = "/html/body/div[1]/main/div[4]/div[2]/div/div[1]/div/div/header/div[2]/div/span"
    xpath_attack_vector = "/html/body/div[1]/main/div[4]/div[2]/div/div[1]/div/div/ul/li[1]/div[2]"
    xpath_cvss_vector = "/html/body/div[1]/main/div[4]/div[2]/div/div[1]/div/div/footer/div"
    xpath_attack_complexity = "/html/body/div[1]/main/div[4]/div[2]/div/div[1]/div/div/ul/li[2]/div[2]"
    xpath_priveleges_required = "/html/body/div[1]/main/div[4]/div[2]/div/div[1]/div/div/ul/li[3]/div[2]"
    xpath_user_interaction = "/html/body/div[1]/main/div[4]/div[2]/div/div[1]/div/div/ul/li[4]/div[2]"
    xpath_scope = "/html/body/div[1]/main/div[4]/div[2]/div/div[1]/div/div/ul/li[5]/div[2]"
    xpath_confidentiality = "/html/body/div[1]/main/div[4]/div[2]/div/div[1]/div/div/ul/li[6]/div[2]"
    xpath_integrity = "/html/body/div[1]/main/div[4]/div[2]/div/div[1]/div/div/ul/li[7]/div[2]"
    xpath_availability = "/html/body/div[1]/main/div[4]/div[2]/div/div[1]/div/div/ul/li[8]/div[2]"
    xpath_credit = "/html/body/div[1]/main/div[4]/div[2]/div/div[2]/div/dl/dd[1]"
    xpath_cwe_url = "/html/body/div[1]/main/div[4]/div[2]/div/div[2]/div/dl/dd[3]/a"
    xpath_cve_url = "/html/body/div[1]/main/div[4]/div[2]/div/div[2]/div/dl/dd[2]/a"
    xpath_snyk_id = "/html/body/div[1]/main/div[4]/div[2]/div/div[2]/div/dl/dd[4]"
    xpath_disclosed = "/html/body/div[1]/main/div[4]/div[2]/div/div[2]/div/dl/dd[5]"
    xpath_published = "/html/body/div[1]/main/div[4]/div[2]/div/div[2]/div/dl/dd[6]"
    xpath_card_content = "/html/body/div[1]/main/div[4]/div[1]/div[2]/div/div"

    def next_page_url(self, page: int) -> str:
        return f"https://snyk.io/vuln/page/{page}?type=any"


