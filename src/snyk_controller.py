import re
import time
import random
from typing import Optional
from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.common.keys import Keys
from selenium.common.exceptions import NoSuchElementException

from config import Configuration


class SNYKController:

    def __init__(self):
        self.config = Configuration()
        self.options = webdriver.ChromeOptions()
        self.options.add_argument('--headless')
        self.options.add_argument('--no-sandbox')
        self.options.add_argument('--disable-dev-shm-usage')
        self.browser = webdriver.Chrome(ChromeDriverManager().install(), options=self.options)
        self.snyk_database = []
        self.page_number = 1
        self.processing = True

    def update_all(self):
        while self.processing:
            current_page_link = self.config.next_page_url(self.page_number)
            if self.__load_browser_url(current_page_link):
                if not self.__parse_page():
                    self.processing = False
                self.page_number += 1
            else:
                self.processing = False
        self.__close_browser()

    def __parse_page(self) -> list:
        page_links = self.__get_all_page_links()
        for page_link in page_links:
            if page_link and self.__load_browser_url(page_link):
                print("*****************************************************************************")
                print("Page Link:       ", page_link)
                print("*****************************************************************************")
                page_content = self.__parse_vulnerability_page()
                page_content["url"] = page_link
                if page_content["package_name"] != "":
                    page_content["original_versions"] = self.__clear_original_versions(page_content["versions"])
                    page_content["versions"] = self.__parse_versions(page_content["versions"])
                    parsed_card_content = self.__parse_card_content(page_content["card_content"])
                    page_content["overview"] = parsed_card_content["overview"]
                    page_content["remediation"] = parsed_card_content["remediation"]
                    page_content["references"] = parsed_card_content["references"]
                    print("Package Name:    ", page_content["package_name"])
                    print("Package version: ", page_content["versions"])
                    print("Original version:", page_content["original_versions"])
                    print("CVSS Score:      ", page_content["cvss_crore"])
                    print("CVSS Vector:     ", page_content["cvss_vector"])
                    print("References:      ", page_content["references"])
                    print("overview:      ", page_content["overview"])
                    print("remediation:      ", page_content["remediation"])
                    self.snyk_database.append(page_content)
                else:
                    print("Empty page or just a link to other pages")
        return page_links

    def __get_element_text_if_exist(self, element_xpath):
        element = self.__get_element(element_xpath)
        return element.text.strip() if element is not None else ""

    def __parse_vulnerability_page(self):
        template = {}
        template["package_name"] = self.__get_element_text_if_exist(self.config.xpath_package_name)
        template["versions"] = self.__get_element_text_if_exist(self.config.xpath_versions)
        template["cvss_crore"] = self.__get_element_text_if_exist(self.config.xpath_cvss_crore)
        template["cvss_vector"] = self.__get_element_text_if_exist(self.config.xpath_cvss_vector)
        template["severity"] = self.__get_element_text_if_exist(self.config.xpath_severity)
        template["attack_vector"] = self.__get_element_text_if_exist(self.config.xpath_attack_vector)
        template["attack_complexity"] = self.__get_element_text_if_exist(self.config.xpath_attack_complexity)
        template["priveleges_required"] = self.__get_element_text_if_exist(self.config.xpath_priveleges_required)
        template["user_interaction"] = self.__get_element_text_if_exist(self.config.xpath_user_interaction)
        template["scope"] = self.__get_element_text_if_exist(self.config.xpath_scope)
        template["confidentiality"] = self.__get_element_text_if_exist(self.config.xpath_confidentiality)
        template["integrity"] = self.__get_element_text_if_exist(self.config.xpath_integrity)
        template["availability"] = self.__get_element_text_if_exist(self.config.xpath_availability)
        template["credit"] = self.__get_element_text_if_exist(self.config.xpath_credit)
        template["cwe"] = self.__get_element_text_if_exist(self.config.xpath_cwe_url)
        template["cve"] = self.__get_element_text_if_exist(self.config.xpath_cve_url)
        template["snyk_id"] = self.__get_element_text_if_exist(self.config.xpath_snyk_id)
        template["disclosed"] = self.__get_element_text_if_exist(self.config.xpath_disclosed)
        template["published"] = self.__get_element_text_if_exist(self.config.xpath_published)
        template["card_content"] = self.__get_element(self.config.xpath_card_content)
        return template

    @staticmethod
    def __parse_versions(versions: str) -> list:
        if versions.strip() in ["", "ALL", "0"]:
            return ["*"]
        else:
            p = versions.replace(" ", ",")
            p = "".join([i for i in p if i.isdigit() or i in [".", ","]])
            p = p.split(",")
            p = [i for i in p if i]
            return p

    @staticmethod
    def __clear_original_versions(versions: str) -> str:
        if versions.strip() in ["", "ALL", "0"]:
            return "*"
        return versions

    def __parse_card_content(self, content):
        result = {"overview": [], "remediation": [], "references": []}
        if content and content.text:
            content_text = content.text
        else:
            content_text = ""

        _content = [c.strip() for c in content_text.split()]

        state = "start"

        for word in _content:
            if word == "Overview":
                state = "overview"
                continue

            if state == "overview":
                if word == "Remediation":
                    state = "remediation"
                else:
                    result["overview"].append(word)
            elif state == "remediation":
                if word == "References":
                    state = "references"
                else:
                    result["remediation"].append(word)

        try:
            hrefs = content.find_elements_by_tag_name("a")
        except Exception:
            hrefs = []

        result["references"] = [item.get_attribute('href') for item in hrefs]
        result["overview"] = " ".join(result["overview"])
        result["remediation"] = " ".join(result["remediation"])

        return result

    def __get_all_page_links(self) -> list:
        hrefs = self.browser.find_elements_by_tag_name("a")
        page_links = [item.get_attribute('href') for item in hrefs if item.get_attribute("href").startswith("https://snyk.io/vuln/") and not item.get_attribute('href').startswith("https://snyk.io/vuln/page/") ]
        for link in page_links:
            print("Got link: ", link)
        return page_links

    def __sleep(self, seconds=None) -> None:
        if seconds is None:
            time.sleep(random.randrange(self.config.min_sleep_time, self.config.max_sleep_time))
        else:
            time.sleep(seconds)

    def __get_element(self, xpath):
        if self.browser:
            try:
                return self.browser.find_element_by_xpath(xpath)
            except Exception:
                pass

    def __close_browser(self) -> None:
        if self.browser:
            self.browser.close()
            self.browser.quit()

    def __load_browser_url(self, url) -> bool:
        count = 0
        while count < self.config.retry_count:
            if self.browser:
                try:
                    self.browser.get(url)
                    self.__sleep()
                    return True
                except Exception:
                    self.__sleep(self.config.huge_sleep_time)
                    count += 1
        return False

